package org.jala.coffeemaker.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BoilerSensorTest {
    @Test
    void testBoilerEmpty() {
        WaterContainer container = new WaterContainer(100);
        BoilerSensor sensor = new BoilerSensor(container);
        assertEquals(sensor.sense(), BoilerStatus.BOILER_EMPTY);
    }

    @Test
    void testBoilerNotEmpty() {
        WaterContainer container = new WaterContainer(100);
        BoilerSensor sensor = new BoilerSensor(container);
        container.fillWater(20);
        assertEquals(sensor.sense(), BoilerStatus.BOILER_NOT_EMPTY);
    }
}