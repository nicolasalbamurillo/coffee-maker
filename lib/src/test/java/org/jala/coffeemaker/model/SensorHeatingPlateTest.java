package org.jala.coffeemaker.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SensorHeatingPlateTest {
    @Test
    void testPotNotEmpty() {
        RemovableWaterContainer container = new RemovableWaterContainer(100);
        SensorHeatingPlate sensor = new SensorHeatingPlate(container);
        assertEquals(sensor.sense(), HeatingPlateStatus.POT_EMPTY);
    }

    @Test
    void testPotEmpty() {
        RemovableWaterContainer container = new RemovableWaterContainer(100);
        SensorHeatingPlate sensor = new SensorHeatingPlate(container);
        container.fillWater(20);
        assertEquals(sensor.sense(), HeatingPlateStatus.POT_NOT_EMPTY);
    }

    @Test
    void testWarmerEmpty() {
        RemovableWaterContainer container = new RemovableWaterContainer(100);
        SensorHeatingPlate sensor = new SensorHeatingPlate(container);
        container.remove();
        assertEquals(sensor.sense(), HeatingPlateStatus.WARMER_EMPTY);
    }
}