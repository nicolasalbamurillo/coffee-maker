package org.jala.coffeemaker.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HeatingPlateTest {
    @Test
    void testTurnOnHeaterShouldBeHeating() {
        HeatingPlate heatingPlate = new HeatingPlate(new RemovableWaterContainer());
        heatingPlate.turnOnHeater();
        assertTrue(heatingPlate.isHeating());
    }

    @Test
    void testTurnOnHeateShouldBeNotHeating() {
        HeatingPlate heatingPlate = new HeatingPlate(new RemovableWaterContainer());
        heatingPlate.turnOnHeater();
        heatingPlate.turnOffHeater();
        assertFalse(heatingPlate.isHeating());
    }
}