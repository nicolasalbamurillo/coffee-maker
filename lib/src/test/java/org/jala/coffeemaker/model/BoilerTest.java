package org.jala.coffeemaker.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoilerTest {
    @Test
    void testTurnOnHeaterShouldTurOnHeater() {
        Boiler boiler = new Boiler();
        boiler.turnOnHeater();
        assertTrue(boiler.isHeating());
    }

    @Test
    void testTurnOffHeaterShouldTurnOffHeaterWhenHeaterIsTurnOn() {
        Boiler boiler = new Boiler();
        boiler.turnOnHeater();
        boiler.turnOffHeater();
        assertFalse(boiler.isHeating());
    }
}