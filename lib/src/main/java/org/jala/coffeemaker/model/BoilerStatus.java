package org.jala.coffeemaker.model;

public enum BoilerStatus {
    BOILER_EMPTY,
    BOILER_NOT_EMPTY
}
