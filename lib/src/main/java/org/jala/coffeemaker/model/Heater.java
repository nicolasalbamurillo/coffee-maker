package org.jala.coffeemaker.model;

public interface Heater {
    void turnOnHeater();
    void turnOffHeater();
    boolean isHeating();
}
