package org.jala.coffeemaker.model;

public class Boiler implements Heater {
    BoilerSensor boilerSensor;
    WaterContainer waterContainer;
    private boolean heating;

    public Boiler() {
        this.waterContainer = new WaterContainer();
        this.boilerSensor = new BoilerSensor(this.waterContainer);
    }

    @Override
    public void turnOnHeater() {
        heating = true;
    }

    @Override
    public void turnOffHeater() {
        heating = false;
    }

    @Override
    public boolean isHeating() {
        return heating;
    }
}
