package org.jala.coffeemaker.model;

public class RemovableWaterContainer extends WaterContainer {
    private boolean removed;

    public RemovableWaterContainer() {
        super();
    }

    public RemovableWaterContainer(double maxCapacity) {
        super(maxCapacity);
    }

    public boolean isRemoved() {
        return removed;
    }

    public void remove() {
        this.removed = true;
    }

    public void put() {
        this.removed = false;
    }
}
