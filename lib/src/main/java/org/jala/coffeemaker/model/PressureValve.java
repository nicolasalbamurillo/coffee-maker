package org.jala.coffeemaker.model;

public class PressureValve {
    private boolean closed;

    public void openValve() {
        closed = false;
    }
    public void closeValve() {
        closed = true;
    }
    public boolean isClosed() {
        return closed;
    }
}
