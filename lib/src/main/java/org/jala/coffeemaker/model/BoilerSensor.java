package org.jala.coffeemaker.model;

public class BoilerSensor implements Sensor<BoilerStatus> {
    private final WaterContainer waterContainer;

    public BoilerSensor(WaterContainer waterContainer) {
        this.waterContainer = waterContainer;
    }

    @Override
    public BoilerStatus sense() {
        if (waterContainer.isEmpty()) {
            return BoilerStatus.BOILER_EMPTY;
        }
        return BoilerStatus.BOILER_NOT_EMPTY;
    }
}
