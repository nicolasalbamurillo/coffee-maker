package org.jala.coffeemaker.model;

public class HeatingPlate implements Heater {
    private SensorHeatingPlate sensorHeatingPlate;
    private boolean heating;

    public HeatingPlate(RemovableWaterContainer container) {
        this.sensorHeatingPlate = new SensorHeatingPlate(container);
    }

    @Override
    public void turnOnHeater() {
        heating = true;
    }

    @Override
    public void turnOffHeater() {
        heating = false;
    }

    @Override
    public boolean isHeating() {
        return heating;
    }
}
