package org.jala.coffeemaker.model;

public class SensorHeatingPlate implements Sensor<HeatingPlateStatus> {
    private final RemovableWaterContainer container;

    public SensorHeatingPlate(RemovableWaterContainer container) {
        this.container = container;
    }

    public HeatingPlateStatus sense() {
        if (container.isRemoved()) {
            return HeatingPlateStatus.WARMER_EMPTY;
        } else if (container.isEmpty()) {
            return HeatingPlateStatus.POT_EMPTY;
        }
        return HeatingPlateStatus.POT_NOT_EMPTY;
    }
}
