package org.jala.coffeemaker.model;

public class WaterContainer {

    private double milliliters;
    private final double maxCapacity;
    private static final int MAX_CUPS = 12;
    private static final double MAX_WATER_CAPACITY_IN_ML = 236.58D * MAX_CUPS;

    public WaterContainer() {
        this.maxCapacity = MAX_WATER_CAPACITY_IN_ML;
    }

    public WaterContainer(double maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public void fillWater(double milliliters) {
        if (milliliters + this.milliliters > maxCapacity) {
            throw new IllegalArgumentException();
        }
        this.milliliters += milliliters;
    }

    public double removeWater(double milliliters) {
        if (this.milliliters - milliliters < this.milliliters){
            throw new IllegalArgumentException();
        }
        this.milliliters -= milliliters;
        return milliliters;
    }

    public double getMilliliters() {
        return milliliters;
    }

    public double getMaxCapacity() {
        return maxCapacity;
    }

    public boolean isEmpty() {
        return milliliters <= 0;
    }
}
