package org.jala.coffeemaker.model;

public interface Sensor<T> {
    T sense();
}
