package org.jala.coffeemaker.model;

public class CoffeeMarker {
    private Boiler boiler;
    private PreparationButton button;
    private PressureValve pressureValve;
    private HeatingPlate heatingPlate;

    public CoffeeMarker(Boiler boiler, PreparationButton button, PressureValve pressureValve, HeatingPlate heatingPlate) {
        this.boiler = boiler;
        this.button = button;
        this.pressureValve = pressureValve;
        this.heatingPlate = heatingPlate;
    }

    public void start() {
        pressureValve.closeValve();
        boiler.turnOnHeater();
    }

    public void stop() {
        pressureValve.openValve();
    }

    public void complete() {
        button.turnOnLight();
        boiler.turnOffHeater();
    }
}
