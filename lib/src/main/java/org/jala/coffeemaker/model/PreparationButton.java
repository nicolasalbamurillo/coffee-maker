package org.jala.coffeemaker.model;

public class PreparationButton {

    private boolean isPressed;
    private boolean lightOn;

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public boolean isPressed() {
        return isPressed;
    }
    public void turnOnLight() {
        lightOn = true;
    }
    public void turnOffLight() {
        lightOn = false;
    }

    public boolean isLightOn() {
        return lightOn;
    }
}
